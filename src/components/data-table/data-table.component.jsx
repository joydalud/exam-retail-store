import React, { useState, useEffect } from 'react';
import './data-table.styles.css';
import 'antd/dist/antd.css';
import { Table, Form, Select, Button, Row, Col } from 'antd';
import CURRENCY_DATA from './currency.data.js';

function DataTable() {
	const { Option } = Select;
	const [form] = Form.useForm();
	let list = null;
	const [sales, setSales] = useState(null);
	const [isLoaded, setIsLoaded] = useState(false);
	const [stores, setStores] = useState(null);
	const [countries, setCountries] = useState(null);
	const [storeSelected, setStoreSelected] = useState(null);
	const [countrySelected, setCountrySelected] = useState(null);
	const columns = [
		{
			title: 'Country',
			dataIndex: 'country',
			key: 'country'
		},
		{
			title: 'Store',
			dataIndex: 'store',
			key: 'store',
			sorter: (a, b) => a.store.length - b.store.length,
		},
		{
			title: 'Value',
			dataIndex: 'value',
			key: 'value',
			sorter: (a, b) => a.value - b.value,
			render: (value, record) => {
				const currency = CURRENCY_DATA.filter((currency) => {
					return currency.code === record.country
				})
				return (
					<p>{currency[0].symbol}{value}</p>
				)
			}
		}		
	];

	useEffect(() => {
		fetch("http://www.mocky.io/v2/5d4caeb23100000a02a95477")
			.then(res => res.json())
			.then((result) => {
				list = result.filter((item) => {
					return item.returned === false
				});
				list = list.sort((a, b) => (a.country > b.country ? 1 : -1));

				if(countrySelected !== null){
					list = list.filter((item) => {
						return item.country === countrySelected
					});
				}

				if(storeSelected !== null){
					list = list.filter((item) => {
						return item.store === storeSelected
					});
				}
				
				let countries = [];
				result.map((item)=>{
					if(!countries.includes(item.country)){
						countries.push(item.country)
					}	
				});

				let stores = [];
				result.map((item)=>{
					if(!stores.includes(item.store)){
						stores.push(item.store)
					}	
				});

				setSales(list);
				setStores(stores);
				setCountries(countries);
				setIsLoaded(true);
			})			
	},[storeSelected, countrySelected])
	
	const resetList = () => {
		setCountrySelected(null);
		setStoreSelected(null);

		form.setFieldsValue({
			country: null,
			store: null
		})
	};

	if(isLoaded){
		const storeOption = (
							stores.map((item, index) => {
								return <Option key={index} value={item}>{item}</Option>
							})
						)
		const countryOption = (
							countries.map((item, index) => {
								return <Option key={index} value={item}>{item}</Option>
							})
						)
		return(
			<div id="table-wrapper">
				<Form form={form}>
					<Row gutter={24}>
						<Col span={10}>
							<Form.Item name="country" label="Country">
								<Select
									placeholder="select country"
									style={{ width: 140 }}
									onChange={(e)=>{
										setCountrySelected(e)
									}}
								>
									{countryOption}
								</Select>				
							</Form.Item>
						</Col>
						<Col span={10}>
							<Form.Item name="store" label="Store">
								<Select
									placeholder="select store" 
									style={{ width: 140 }}
									onChange={(e)=>{
										setStoreSelected(e)
									}}
								>
									{storeOption}
								</Select>				
							</Form.Item>				
						</Col>
						<Col span={4}>
						  <Button type="primary" onClick={resetList}>
							Reset
						  </Button>				
						</Col>
					</Row>
				</Form>

				<Table
				  size="small"
				  columns={columns}
				  dataSource={sales}
				/>
			</div>
		);	
	} else {
		return <div>Loading...</div>;
	}

}

export default DataTable;
