const CURRENCY_DATA = [
    {
        "symbol": "AU$",
        "name": "Australian Dollar",
        "code": "AUS",
    },
	{
        "symbol": "$",
        "name": "US Dollar",
        "code": "USA",
    },
    {
        "symbol": "NZ$",
        "name": "New Zealand Dollar",
        "code": "NZL",
    }
];

export default CURRENCY_DATA;
