import './App.css';

import DataTable from './components/data-table/data-table.component';

function App() {
  return (
    <div className="App">
		<DataTable />
    </div>
  );
}

export default App;